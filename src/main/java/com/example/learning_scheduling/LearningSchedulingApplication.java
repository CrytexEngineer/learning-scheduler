package com.example.learning_scheduling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class LearningSchedulingApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearningSchedulingApplication.class, args);
    }

}
